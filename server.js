#!/usr/bin/env node

var express = require('express');
var fs = require('fs');
var logger = require('./util/logger.js');
require('./util/console-wrapper')(logger);
var SampleApp = function() {

    var self = this;

    self.setupVariables = function() {
        //  Set the environment variables we need.
        self.ipaddress = process.env.OPENSHIFT_NODEJS_IP;
        self.port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
        self.mongourl = process.env.OPENSHIFT_MONGODB_DB_URL || "mongodb://localhost:27017/test";//"mongodb://root:root@novus.modulusmongo.net:27017/w8iTanag"
        console.log("No connect")
        if (typeof self.ipaddress === "undefined") {
            //  Log errors on OpenShift but continue w/ 127.0.0.1 - this
            //  allows us to run/test the app locally.
            console.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
            self.ipaddress = "127.0.0.1";
        };
    };

    self.terminator = function(sig) {
        if (typeof sig === "string") {
            console.log('%s: Received %s - terminating sample app ...',
                Date(Date.now()), sig);
            process.exit(1);
        }
        console.log('%s: Node server stopped.', Date(Date.now()));
    };

    self.setupTerminationHandlers = function() {
        //  Process on exit and signals.
        process.on('exit', function() {
            self.terminator();
        });

        // Removed 'SIGPIPE' from the list - bugz 852598.
        ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
            'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
        ].forEach(function(element, index, array) {
            process.on(element, function() {
                self.terminator(element);
            });
        });
    };

    self.initializeServer = function() {
        var mongoose = require('mongoose');
        var express = require('express');
        var connect = require('connect');
        var app = express();
        var morgan = require('morgan');
        var cookieParser = require('cookie-parser');
        var bodyParser = require('body-parser');
        var session = require('express-session');
        var port = process.env.PORT || 8080;
        var https = require('https');
        var fs = require('fs');

        if (self.mongourl !== undefined)
            mongoose.connect(self.mongourl);
        else {
            mongoose.connect(process.env.OPENSHIFT_MONGODB_DB_URL || 'mongodb://localhost:27017/');
        }

        app.use(express.static(__dirname + '/public'));
        app.use(connect.logger('dev'));
        app.use(connect.json());
        app.use(connect.urlencoded());
        app.use(cookieParser()); // read cookies (needed for auth)
        app.use(require('connect').bodyParser()); // get information from html forms
        app.use(require('morgan')({format: 'metrics [:date[clf]] :method :url :response-time :remote-addr',"stream": logger.stream  }));
        var privateKey  = null;
        var certificate = null;
        var credentials = null;

        if(!process.env.NODE) {
            privateKey  = fs.readFileSync('/srv/www/gatherspot/shared/config/ssl.key', 'utf8');
            certificate = fs.readFileSync('/srv/www/gatherspot/shared/config/ssl.crt', 'utf8');
            credentials = {key: privateKey, cert: certificate};
            self.https = https.createServer(credentials, app);
        }

        require('./controller/post_local')(app);
        require('./controller/post_personal')(app);
        require('./controller/user')(app);
        app.get('/', function(req, res) {
            res.send('<!doctype html><html><head><title>Coeo</title><link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css"> <!-- load bootstrap css -->' + '<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"> <!-- load fontawesome -->' + '<style>' + 'body    { padding-top:80px; }' + '</style>' + '</head>' + '<body>' + '<div class="container">' + '<div class="jumbotron text-center"><h1><span class="fa fa-lock"></span>Making social local</h1>' + '</div>' + '</div></body></html>');
        });
        self.app = app;
        self.app.config = require('./config');
        if(process.env.NODE) {
            app.listen(3000);
        } else {
            self.https = https.createServer(credentials, app);
        }
    };

    self.initialize = function() {
        self.setupVariables();
        self.setupTerminationHandlers();

        self.initializeServer();
    };

    self.start = function() {
        //  Start the app on the specific interface (and port).
        if(!process.env.NODE) {
            self.https.listen(process.env.PORT || 3000, function() {
                console.log('Node server started on port', process.env.PORT || 3000);
             });
        }
        
    };

};
var cluster = require('cluster');
var LOG_STREAM = fs.createWriteStream('./logMain.log', { flags: 'a+'  });
if (cluster.isMaster) {
    var cpuNum = 1;//require('os').cpus().length;
    for (var i = 0; i < cpuNum; i++) {
        cluster.fork();
    }
} else {
    var zapp = new SampleApp();
    zapp.initialize();
    zapp.start();
}

cluster.on('exit', function(worker) {
    // Replace the dead worker,
    // we're not sentimental
    console.log('Worker ' + worker.id + ' died :(');
    cluster.fork();
});
process.stdout.pipe(LOG_STREAM);
console.log('redirect done');

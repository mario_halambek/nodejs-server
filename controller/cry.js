var crypto = require('crypto');
var rand = require('csprng');

module.exports = function(salt, value){
	return crypto.createHash('sha512').update(salt + value).digest("hex");
}

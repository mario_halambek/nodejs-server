var post_local = require('../models/post_local');
var user = require('../models/user');
var mongoose = require('mongoose');
var Image = require('../models/image');
var fs = require("fs");
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var comments = require('../models/comments_personal_posts');
var geocoder = require('geocoder');
var auth = require('./auth.js').auth;
var moment = require('moment');
var Report = require('../models/report');
var gcm = require('../gcm');
var config = require('../config.js');
function diffrence(argument) {
	argument /= -1000;
	if (argument < 60) {
		return Math.floor(argument) + 's';
	} else {
		argument /= 60;
		if (argument < 60) {
			return Math.floor(argument) + 'min';
		} else {
			argument /= 60;
			if (argument < 24) {
				return Math.floor(argument) + 'h';
			} else {
				argument /= 24;
				if (argument < 7) {
					return Math.floor(argument) + 'd';
				} else {
					argument /= 7;
					if (argument < 4) {
						return Math.floor(argument) + 'w';
					} else {
						argument /= 4;
						if (argument < 12) {
							return Math.floor(argument) + 'm';
						} else {
							argument /= 12;
							return Math.floor(argument) + 'y';
						}
					}
				}
			}
		}
	}
}
var _app;

function filterPost(user, post) {
	var a = diffrence(post.timeStamp - new Date());
   if(post.up_votes) console.log(post.up_votes.indexOf(user._id) > -1);
	console.log('up_votes', post.up_votes.some(function(id) {
		return id.equals(user._id);
	}), 'user id', user._id);
	console.log(typeof post.up_votes[0] === typeof user._id);
	return {
		image: post.image_id ? _app.config.serverUrl + '/image/?id=' + post.image_id : null,
		status: post.status,
		user_id: post.user_id,
		timestamp: a,
		votes: (post.up_votes ? post.up_votes.length : 0) - (post.down_votes ? post.down_votes.length : 0),
		upvoted: post.up_votes ? (post.up_votes.some(function(id) {
		return id.equals(user._id);
	})) : false, //true
		downvoted: post.down_votes ? (post.down_votes.some(function(id) {
		return id.equals(user._id);
	})) : false, //false
		comments_number: post.comments_number,
		radius: post.distance,
		raw_date: post.timeStamp,
		id: post._id,
		lat: post.coords[1],
		lng: post.coords[0]
	};
}

var save_error = function(to_save, res, callback) {
	to_save.save(function(err) {
		console.log(err);
		if (err) {
			res.send(500, err);
		} else {
			callback();
		}
	});
};

module.exports = function(app) {
	_app = app;
	app.all('/post_local*', function(req, res, next) {
		// console.log('In auth for post locals');
		auth(req, res, function(req, res, user) {
			// console.log('In auth for post locals', user);
			req.user = user;
			next();
		});
	});

	// app.post('/post/local/new', function(req, res) {
	// 	res.send(req.user);
	// });

	app.post('/post_local/new', multipartMiddleware, function(req, res) {
		{
			if (!req.body.status) {
				res.send(400, "No content_text");
			} else if(req.body.status.length > 2000){
            res.send(400, 'Status too long');
			} else {
				geocoder.reverseGeocode(Number(req.body.y), Number(req.body.x), function(err, data) {
					var post = new post_local({
						status: req.body.status,
						user_id: mongoose.Types.ObjectId(req.user._id),
						coords: [Number(req.body.x), Number(req.body.y)],
						up_votes: [],
						down_votes: [],
						comments_number: 0
					});
					var n;
					// console.log(data.results);
					if (data && data.results.length > 0) {
						n = data.results[0].formatted_address;
						// var name = data.results.pop();
						// if (data.results.length > 2) {
						// 	name = data.results[data.results.length - 1];
						// }
						// n = name.formatted_address.substring(0, name.formatted_address.indexOf(','));
					} else {
						n = 'None';
					}
					console.log(n);
					post.name = n;
					console.log(post.name);
					post.timeStamp = new Date();
					if (req.files)
						if (!req.files.image) {
							save_error(post, res, function() {
								res.send("Post saved");
							});
						} else {
							Image.saveImageHttp(req, function(image) {
								post.image_id = image._id;
								save_error(post, res, function() {
									res.send("Post saved");
								});
							});
						}
				});
			}
		}
	});

	app.post('/post_local/id', function(req, res) {
		if (!req.body.id) {
			res.send(400, {
				err: 'No post id'
			});
		}
		post_local.findById(req.body.id).populate('user_id').exec(function(err, post) {
			if (err || !post) {
				res.send(400, {
					err: err || 'No such post'
				});
				console.log(err || 'No such post');
				return;
			}
			console.log(post);
			var feed = [post].map(filterPost.bind(null, req.user));
			var p = feed.pop();
			p.user_id = post.user_id._id;
			p.username = post.user_id.username || post.user_id.displayName;
			p.profile_pic_url = post.user_id.profile_pic_id ? (config.serverUrl + '/image/?id=' + post.user_id.profile_pic_id) : post.user_id.profile_pic_url;
			res.send(p);
		});
	});

	app.post('/post_local/comments', function(req, res) {
		{
		console.log('Posting local post comment');
			if (!req.body.post_id) {
				res.send(400, "No post id");
			} else {
				post_local.findOne({
					_id: req.body.post_id,
					removed: false
				}).populate('user_id').exec(function(err, post) {
					if (!post) {
						res.send(410, "Post no longer exists");
					} else {
						var comment = new comments({
							post_id: req.body.post_id,
							timestamp: new Date(),
							user_id: req.user._id,
							status: req.body.status
						});
						console.log('Making comment');
						comment.save(function(err) {
							if(err) {
								console.log(err, req.user._id);
								return res.status(500).send();
							}
							console.log('Saved comment');
							post_local.findOneAndUpdate({_id:post._id}, {$inc: {comments_number: 1}}, {multi: false}, function(err, stat) {
								console.log('Update Post',err, stat);
								res.send({id: comment._id});
							});
						});
						// save_error(comment, res, function() {
							// post.comments_number += 1;
							// post.markModifed('comments_number');
							// save_error(post, res, function() {
								// res.send({id: comment._id});
							// });
						// });
						if(!post.user_id._id.equals(req.user._id)) {
							gcm.notifyOwner(req.user, post);
						}
						comments.find({post_id: req.body.post_id}).populate('user_id').exec(function(err, raw_comments) {
							gcm.notifyCommenters(req.user,raw_comments.map(function(comment) {
								return comment.user_id;
							}), post);
						});
					}
				});
			}
		}
	});

	//how to supply comment id to frontend safely
	app.post('/post_local/comments/delete', function(req, res) {
      comments.findOneAndUpdate({removed: false, _id: req.body.id}, {removed: true}, function(err, comment) {
			if(err || !comment) {
				return res.send(400);
			}
			post_local.findOneAndUpdate({_id: comment.post_id}, {$inc: {comments_number: -1}}, {multi: false}, function(err, post) {
				if(err || !post) {
					return res.send(400);
				}
				res.send();
			});
		});
			// post_local.findById(req.body.post_id, function(err, post) {
			// 	if (!post) {
			// 		res.send(410, "Post no longer exists so no comment to remove");
			// 	} else {
			// 		comments.findOne({
			// 			_id: req.body.id,
			// 			user_id: req.user._id
			// 		}, function(err, comment) {
			// 			if (err) {
			// 				res.send(500, err);
			// 			} else if (!comment) {
			// 				res.send(410, "No comment to remove");
			// 			} else {
			// 				comment.removed = true;
			// 				comment.save(function(err) {
			// 					if (err) {
			// 						res.send(500);
			// 					} else {
			// 						post.comments_number -= 1;
			// 						post.save();
			// 						res.send(200);
			// 					}
			// 				});
			// 			}
			// 		});
			// 	}
			// // });
	});

	app.post('/post_local/edit', multipartMiddleware, function(req, res) {
		if (!req.body.id) {
			res.send(400, "No post id");
		} else if (!req.body.status) {
			res.send(400, "No content text");
		} else if(req.body.status.length > 2000){
         res.send(400, 'Status too long');
		} else {
			post_local.findOne({
				_id: req.body.id,
				user_id: req.user_id,
				removed: {
					$in: [null, false]
				}
			}, function(err, post) {
				if (err) {
					res.send(500, err);
				}
				if (!post) {
					res.send(400, {
						err: "no post"
					});
				} else {
					post.status = req.body.status;
					if (post.image) Image.remove({
						_id: post.image
					});
					delete post.image;
					if (!req.files.image) {
						save_error(post, res, function() {
							res.send("Post edited");
						});
					} else {
						Image.saveImageHttp(req, function(image) {
							post.image_id = image._id;
							save_error(post, res, function() {
								res.send("Post edited");
							});
						});
					}
				}
			});
		}
	});

	var meters_magic_number = 6378137;

	var location = {
		type: "Point",
		coordinates: [0, 0]
	};
	var searchOptions = {
		maxDistance: 1000 / meters_magic_number,
		spherical: true
	};

	app.get('/post_local/my', function(req, res) {
		var pageSize = req.query.pageSize || 100;
		var pageNumber = req.query.pageNumber || 0;
      console.debug('pageSize', pageSize, 'pageNumber', pageNumber);
		post_local.find({
			user_id: req.user._id,
			removed: {
				$in: [false, null]
			}
		}).sort({"timeStamp": -1}).skip(pageSize * pageNumber).limit(pageSize).exec(function(err, posts) {
			console.log(err, posts);
			if (err) {
				res.send(500, err);
			} else {
				for (var i = 0; i < posts.length; i++) {
					var tmp = diffrence(posts[i].timeStamp - new Date());
					posts[i] = {
						timeStamp: tmp,
						status: posts[i].status,
						name: posts[i].name,
						comments_number: posts[i].comments_number,
						id: posts[i]._id,
						votes: posts[i].up_votes.length - posts[i].down_votes.length,
						image: posts[i].image_id ? app.config.serverUrl + "/image/?id=" + posts[i].image_id : null,
						lat: posts[i].coords[1],
						lng: posts[i].coords[0]
					};
				}
				res.send(posts);
			}
		});
	});

	app.post('/post_local/up_vote', function(req, res) {
		if (!req.body.id) {
			res.send(400, {
				'err': "No post id"
			});
		} else {
			post_local.findOne({
				_id: req.body.id,
				removed: {
					$in: [null, false]
				}
			}, function(err, post) {
				if (err) {
					res.send(500, err);
				} else if (!post) {
					res.send(400, {
						"err": "No post"
					});

					//use indexOf only once
				} else if (post.up_votes.indexOf(req.user._id) != -1) {
					//remove up vote
					post.up_votes.splice(post.up_votes.indexOf(req.user._id), 1);
					save_error(post, res, function() {
						res.send({
							response: "not up"
						});
					});
				} else {
					post.up_votes.push(req.user._id);
					save_error(post, res, function() {
						res.send({
							response: "up"
						});
					});
				}
			});
		}
	});

	app.post('/post_local/down_vote', function(req, res) {
		if (!req.body.id) {
			res.send(400, "No post id");
		} else {
			post_local.findOne({
				_id: req.body.id,
				removed: {
					$in: [null, false]
				}
			}, function(err, post) {
				if (err) {
					res.send(500, err);
				} else if (!post) {
					res.send(400, "No such post");
					//use indexOf only once
				} else if (post.down_votes.indexOf(req.user._id) != -1) {
					post.down_votes.splice(post.up_votes.indexOf(req.user._id), 1);
					save_error(post, res, function() {
						res.send({
							response: "not down"
						});
					});
				} else {
					post.down_votes.push(req.user._id);
					save_error(post, res, function() {
						res.send({
							response: "down"
						});
					});
				}
			});
		}
	});

	//clean up others
	app.post('/post_local/delete', function(req, res) {
		auth(req, res, function(req, res, user) {
			post_local.findOne({
				_id: req.body.id,
				user_id: req.user._id
			}, function(err, post) {
				if (err) {
					res.send(500);
				} else if (!post) {
					res.send(400, "No such post");
				} else {
					post.removed = true;
					post.save(function(err) {
						if (err) {
							res.send(500);
						} else {
							res.send(200);
						}
					});
				}
			});
		});
	});

	app.post('/post_local/comments/get', function(req, res) {
		var pageSize = 100;
		var pageNumber = 0;

		if (req.body.pageSize) {
			pageSize = Number(req.body.pageSize)
		}
		if (req.body.pageNumber) {
			pageNumber = Number(req.body.pageNumber)
		}
		if (!req.body.post_id) {
			res.send(400);
		} else {
			comments.find({
				post_id: req.body.post_id,
				removed: {
					$in: [false, null]
				}
			}).sort('-timestamp').skip(pageSize * pageNumber).limit(pageSize).populate('user_id').select({
				post_id: 1,
				user_id: 1,
				status: 1,
				timestamp: 1
			}).exec(function(err, found) {
				if (err) {
					res.send(500);
				} else if (!found) {
					res.send(404);
				} else {
					//TODO facebook
					res.send({
						comments: found.map(function(obj) {
							var ret = {
								post_id: obj.post_id,
								status: obj.status,
								timestamp: diffrence(obj.timestamp - new Date()),
								id: obj._id
							};
							if (!obj.user_id.facebookId) {
								ret.user_id = {
									user_id: obj.user_id._id,
									username: obj.user_id.username || obj.user_id.displayName,
									profile_pic_url: app.config.serverUrl + '/image/?id=' + obj.user_id.profile_pic_id
								};
							} else {
								ret.user_id = {
									user_id: obj.user_id._id,
									username: obj.user_id.username || obj.user_id.displayName,
									profile_pic_url: obj.user_id.profile_pic_url
								};
								if (obj.user_id.profile_pic_id) {
									ret.user_id.profile_pic_url = app.config.serverUrl + '/image/?id=' + obj.user_id.profile_pic_id;
								}
							}
							return ret;
						})
					});
				}
			});
		}
	});

	app.get('/post_local', multipartMiddleware, function(req, res) {
		var location = {
			type: "Point",
			coordinates: [Number(req.query.x), Number(req.query.y)]
		};
		console.log(req.query);
		var pageSize = 100,
			pageNumber = 0;
		if (req.query.pageSize) {
			pageSize = Number(req.query.pageSize);
		}
		if (req.query.pageNumber) {
			pageNumber = Number(req.query.pageNumber);
		}
		var radius = 1000;
		if (req.query.radius) {
			radius = Number(req.query.radius);
		}
		searchOptions.maxDistance = Number(req.query.radius) / meters_magic_number;
		console.log(pageNumber * pageSize);
		console.log(pageNumber, pageSize);
		post_local.aggregate(
			[{
				"$geoNear": {
					"near": {
						"type": "Point",
						"coordinates": [parseFloat(req.query.x), parseFloat(req.query.y)]
					},
					"distanceField": "distance",
					"maxDistance": radius,
					"spherical": true,
				}
			}, {
				"$sort": {
					"timeStamp": -1
				}
			}, {
				"$project": {
					timeStamp: 1,
					image_id: 1,
					status: 1,
					user_id: 1,
					comments_number: 1,
					distance: 1,
					removed: 1,
					coords: 1,
					up_votes: 1,
					down_votes: 1
				}
			}, {
				$match: {
					removed: {
						$in: [false, null]
					}
				}
			}, {
				$skip: pageNumber * pageSize
			}, {
				$limit: pageSize
			}],
			function(err, posts) {
				console.log(posts);
				if (err) {
					res.send(500, err);
					console.log(err);
				} else {
					console.info('USER', JSON.stringify(req.user));
					var feed = posts.map(filterPost.bind(null,req.user));
					var userIds = posts.map(function(post) {
						return post.user_id;
					});
					user.find({
						_id: {
							$in: userIds
						}
					}, function(err, users) {
						if (err) {
							res.send(500, err);
						} else {
							users = users.reduce(function(prev, curr, index, array) {
								//TODO facebook
								if (curr.facebookId) {
									prev[curr._id] = {
										username: curr.displayName
									};
									if (curr.profile_pic_id) {
										prev[curr._id].profile_pic_url = curr.profile_pic_id ? app.config.serverUrl + '/image/?id=' + curr.profile_pic_id : null;
									} else {
										prev[curr._id].profile_pic_url = curr.profile_pic_url;
									}
								} else {
									prev[curr._id] = {
										username: curr.username || curr.displayName,
										profile_pic_url: curr.profile_pic_id ? app.config.serverUrl + '/image/?id=' + curr.profile_pic_id : null
									};
								}
								return prev;
							}, {});
							res.send({
								feed: feed,
								users: users
							});
						}
					});
				}
			});
	});

	var SimpleCache = require("simple-lru-cache");

	var cache = new SimpleCache({
		"maxSize": 50
	});


	//to add auth for image fetch
	app.get('/image', function(req, res) {
		if (req.query.id) {
			var tmp = cache.get(req.query.id);
			res.setHeader("Cache-Control", "public, max-age=100000");
			if (tmp) {
				res.contentType(tmp.contentType);
				res.send(tmp.data);
			} else {
				Image.findById(req.query.id, function(err, post) {
					if (!post) {
						res.send(404);
					} else {
						cache.set(req.query.id, post);
						res.contentType(post.contentType);
						res.send(post.data);
					}
				});
			}
		} else {
			res.send(400);
		}
	});

	app.post('/post_local/report/comment', function(req, res) {
		comments.findById(req.body.id, function(err, comment) {
			if (err) {
				res.send(500);
			} else if (!comment) {
				res.send(400);
			} else {
				Report.count({
					user_id: req.user._id,
					post_id: comment.post_id
				}, function(err, reportCount) {
					if (err) {
						res.send(500);
					} else if (reportCount > 0) {
						res.send(400, {
							status: "Already reported"
						});
					} else {
						Report.create({
							user_id: req.user.id,
							post_id: comment._id,
							reason: req.body.reason
						}, function(err, report) {
							Report.count({
								post_id: comment._id
							}, function(err, reportCount) {
								if (reportCount === 10) {
									comment.removed = true;
									comment.save(function(err, comment) {
										if (err) {
											res.send(500);
										} else {
											res.send({
												status: "comment reported"
											});
										}
									});
								} else {
									res.send({
										status: "comment reported"
									});
								}
							});
						});
					}
				});
			}
		});
	});

	app.post('/post_local/report', function(req, res) {
		post_local.findById(req.body.post_id, function(err, post) {
			if (err) {
				res.send(500);
			} else if (!post) {
				res.send(400);
			} else {
				Report.count({
					user_id: req.user._id,
					post_id: post._id
				}, function(err, count) {
					if (err) {
						res.send(500)
					} else if (count === 0) {
						Report.create({
							user_id: req.user._id,
							post_id: post._id,
							reason: req.body.reason
						}, function(err, report) {
							if (err) {
								res.send(500);
							} else if (!Report) {
								res.send(400);
							} else {
								res.send({
									status: 'Report submited'
								});
								Report.count({
									post_id: post._id
								}, function(err, count) {
									if (err) {
										console.log(err)
									} else if (count >= 10) {
										post.removed = true;
										post.save(function(err) {
											if (err) {
												console.log(err);
											} else {
												console.log('Remove post', JSON.stringify(post));
											}
										})
									}
								})
							}
						});
					} else {
						res.send(400, {
							status: "Already reported"
						});
					}
				})
			}
		});
	})

};

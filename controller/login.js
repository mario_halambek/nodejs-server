var crypto = require('crypto');
var rand = require('csprng');
var mongoose = require('mongoose');
var User = require('../models/user');
var FB = require('fb');
var hasher = require('./cry');

exports.loginPassword = function(email, password, gcmToken, res) {
	User.findOne({
		email: email,
		deleted: false
	}, function(err, user) {
		if(err) {
			res.status(500).send();
		}
		if (user) {
			var temp = user.salt;
			var hash_db = user.hashed_password;
			var id = user.token;
			var newpass = temp + password;
			var hashed_password = hasher(user.salt, password);
			var first = true;
			if(user.last_login_date) {
				first = false;
			}
			user.last_login_date = new Date();
			user.markModified('last_login_date');
			if(gcmToken && user.gcmToken.indexOf(gcmToken) === -1) {
				user.gcmToken.push(gcmToken);
				user.markModified('gcmToken');
			}
			user.save(function(err) {
				if (err) {
					res.writeHead(500, 'Failled to set date');
					res.end();
				} else {
					console.log('hash_db', hash_db, 'hashed_password', hashed_password);
					if (hash_db === hashed_password) {
                  user.token = crypto.createHash('sha512').update(email + rand(160, 36)).digest('hex');
						user.markModified('token');
						user.save(function(err) {
                     if(err) {
								console.err(err);
								return res.status(500).send();
							}
							res.json({
								'response': "Login Success",
								'res': true,
								'token': user.token,
								'user_id': user._id,
								firstLogin: first
							});
						});
					} else {
						res.writeHead(400, {
							'response': "Invalid Password",
							'res': false
						});
						res.end();
					}
				}
			});

		} else {
			res.writeHead(400, {
				'response': "User not exist",
				'res': false
			});
			res.end();
		}
	});
};

exports.loginFacebook = function(accessToken, gcmToken, res) {
	if (!accessToken) {
		return res.send(400, {
			err: "no facebook accessToken"
		});
	}
	FB.setAccessToken(accessToken);
	FB.api('/me', function(facebookResponse) {
		if (facebookResponse.error) {
			return res.send(400, facebookResponse.error);
		}
		User.findOne({
			facebookId: facebookResponse.id
		}, function(err, user) {
			if (err) {
				console.err(err);
				return res.send(500, {
					err: err
				});
			}
			if (!user) {
				return res.send(402, {
					err: 'No such user'
				});
			}
			var first = true;
			if(user.last_login_date) {
				first = false;
			}
			if(gcmToken && user.gcmToken.indexOf(gcmToken) === -1) {
				user.gcmToken.push(gcmToken);
				user.markModified('gcmToken');
			}
				user.last_login_date = Date.now();
				user.markModified('last_login_date');
				user.save(function(err) {
					if(err) {
						console.err(err);
						res.status(500).send();
						return;
					}
					user.token = crypto.createHash('sha512').update(user.facebookId + rand(160, 36)).digest('hex');
					user.markModified('token');
					user.save(function(err) {
                  if(err) {
							console.err(err);
							return res.status(500).send();
						}
						res.json({
							'response': "Login Success",
							'res': true,
							'token': user.token,
							'user_id': user._id,
							firstLogin: first
						});
					});
			});
			// } else {
			// 	res.json({
			// 		'response': "Login Success",
			// 		'res': true,
			// 		'token': user.token,
			// 		'user_id': user._id
			// 	});
			// }
		});
	});
};

var PostLocal = require('../models/post_local');
var User = require('../models/user');
var Image = require('../models/image');
var Multipart = require('connect-multiparty');
var multipartMiddleware = Multipart();
var auth = require('./auth.js').auth;
var Register = require('./register');
var Login = require('./login');
var Feedback = require('../models/feedback.js');
var mongoose = require('mongoose');
var getPanorama = require('../util/panorama_getter.js');

module.exports = function(app) {
	app.post('/register', function(req, res) {
		if (req.body.grant_type === 'password') {
			Register.registerPassword(req.body.email, req.body.password, req.body.gcmToken, res);
		} else if (req.body.grant_type === 'facebook') {
			Register.registerFacebook(req.body.accessToken, req.body.gcmToken, res);
		} else {
			res.send(400);
		}
	});

	app.post('/login', function(req, res) {
		console.log(req.body.gcmToken);
		if (req.body.grant_type === 'password') {
			Login.loginPassword(req.body.email, req.body.password, req.body.gcmToken, res);
		} else if (req.body.grant_type === 'facebook') {
			Login.loginFacebook(req.body.accessToken, req.body.gcmToken, res);
		} else {
			res.send(400);
		}

	});

	app.all('/user/*', function(req, res, next) {
		auth(req, res, function(req, res, user) {
			req.user = user;
			next();
		});
	});

	var Notification = mongoose.model('push');
	app.post('/user/seen', function(req, res) {
		console.log(req.body);
		if (req.body.id) {
			Notification.remove({
				postId: req.body.id,
				userId: req.user._id
			}, function(err) {
				if (err) {
					console.err(err);
					res.status(500, {
						err: err
					});
				} else {
					res.send({});
				}
			});
		} else {
			res.status(400, {
				err: 'No id'
			});
		}
	});

	app.post('/user/beat', function(req, res) {
		var user = req.user;
		user.coords = [Number(req.body.x), Number(req.body.y)];
		user.last_login_date = new Date();
		user.markModified('last_login_date');
		user.save(function(err) {
			if (err) {
				res.status(500, err);
			} else {
				getPanorama(user.coords, function(url) {
					console.log(url);
					res.send({
						url: url
					});
				});
			}
		});

	});

	app.post('/user/profilepic', multipartMiddleware, function(req, res) {
		if (!req.files.image) {
			res.send(400);
		} else {
			Image.saveImageHttp(req, function(image) {
				req.user.profile_pic_id = image._id;
				req.user.save(function(err) {
					if (err) {
						next(err);
					} else {
						res.send(200);
					}
				});
			});
		}
	});

	app.post('/user/profilepic/get', function(req, res) {
		if(!req.body.user_id) {
			res.status(400).send();
			return;
		}
		User.findById(req.body.user_id, function(err, found) {
			if (err) {
				res.status(400, err);
			} else if (found) {
				if (found.facebookId) {
					if (found.profile_pic_id) {
						res.send(200, {
							username: found.displayName,
							profile_pic_url: found.profile_pic_id ? (app.config.serverUrl + '/image/?id=' + found.profile_pic_id) : null,
							about: found.about || null
						});
					} else {
						res.send({
							username: found.displayName,
							profile_pic_url: found.profile_pic_url,
							about: found.about
						});
					}
				} else {
					res.send(200, {
						username: found.displayName,
						profile_pic_url: found.profile_pic_id ? (app.config.serverUrl + '/image/?id=' + found.profile_pic_id) : null,
						about: found.about || null
					});
				}
			} else {
				res.send({
					err: "no such user"
				});
			}
		});
	});

	app.post('/user/data', function(req, res) {
		if (req.body.gcmToken) {
			req.user.gcmToken = req.body.gcmToken;
		}
		// if (req.user.facebookId) {
			if (req.body.username) req.user.displayName = req.body.username;
			if (req.body.about) req.user.about = req.body.about;
			req.user.save(function(err) {
				if (err) {
					res.status(500, err);
				} else {
					res.send(200);
				}
			});
		// } else {
		// 	User.findOne({
		// 		username: req.body.username
		// 	}, function(err, user) {
		// 		if (err)
		// 			return res.status(500).send({
		// 				err: err
		// 			});
		// 		if (user && req.user.username !== req.body.username)
		// 			return res.status(404).send({
		// 				err: 'Username taken'
		// 			});
		// 		if (req.body.username) req.user.username = req.body.username;
		// 		if (req.body.about) req.user.about = req.body.about;
		// 		req.user.save(function(err) {
		// 			if (err) {
		// 				res.status(500, err);
		// 			} else {
		// 				res.send(200);
		// 			}
		// 		});
		// 	});
		// }
	});

	app.post('/user/feedback', function(req, res) {
		if(!req.body.text) {
			res.status(400).send();
		}
      Feedback.create({text: req.body.text, user: req.user._id}, function(err, feedback) {
			if(err || !feedback) {
				res.status(400).send(err || 'No feedback');
				return;
			}
			res.send();
		});
	});
};

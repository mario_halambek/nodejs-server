var post_personal = require('../models/post_personal');
var User = require('../models/user');
var mongoose = require('mongoose');
var Image = require('../models/image');
var comments = require('../models/comments_personal_posts');
var report = require('../models/report');
var fs = require("fs");
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var auth = require('./auth.js').auth;
var gcm = require('../gcm.js');
var config = require('../config');

var save_error = function(to_save, res, callback) {
	to_save.save(function(err) {
		if (err) {
			console.log(to_save, err);
			res.send(500, err);
		} else {
			callback();
		}
	});
};
var moment = require('moment');

function diffrence(argument) {
	argument /= -1000;
	if (argument < 60) {
		return Math.floor(argument) + 's';
	} else {
		argument /= 60;
		if (argument < 60) {
			return Math.floor(argument) + 'min';
		} else {
			argument /= 60;
			if (argument < 24) {
				return Math.floor(argument) + 'h';
			} else {
				argument /= 24;
				if (argument < 7) {
					return Math.floor(argument) + 'd';
				} else {
					argument /= 7;
					if (argument < 4) {
						return Math.floor(argument) + 'w';
					} else {
						argument /= 4;
						if (argument < 12) {
							return Math.floor(argument) + 'm';
						} else {
							argument /= 12;
							return Math.floor(argument) + 'y';
						}
					}
				}
			}
		}
	}
}

module.exports = function(app) {

	app.all('/post_personal/*', function(req, res, next) {
		auth(req, res, function(req, res, user) {
			req.user = user;
			next();
		});
	});
	app.post('/post_personal/new', multipartMiddleware, function(req, res) {
		if (!req.body.status) {
			res.send(400, "No content_text");
		} else if(req.body.status.length > 2000) {
         res.send(400, 'Status too long');
		} else {
			var post = new post_personal({
				status: req.body.status,
				user_id: req.user._id,
				up_votes: [],
				timeStamp: new Date()
			});
			if (!req.files.image) {
				save_error(post, res, function() {
					res.send("Post saved");
				});
			} else {
				Image.saveImageHttp(req, function(image) {
					post.image_id = image._id;
					save_error(post, res, function() {
						res.send("Post saved");
					});
				});
			}
		}
	});

	app.post('/post_personal/comments/get', function(req, res) {
		var pageSize = 100;
		var pageNumber = 0;
		if (req.body.pageSize) {
			pageSize = Number(req.body.pageSize);
		}
		if (req.body.pageNumber) {
			pageNumber = Number(req.body.pageNumber);
		}
		comments.find({
			post_id: req.body.post_id,
			removed: {
				$in: [false, null]
			}
		}).sort('-timestamp').skip(pageSize * pageNumber).limit(pageSize).populate('user_id').select({
			_id: 1,
			post_id: 1,
			user_id: 1,
			status: 1,
			timestamp: 1
		}).exec(function(err, found) {
			if (err) {
				res.send(500);
			} else if (!found) {
				console.log({
					err: 'No post'
				});
				res.send(404);
			} else {
				console.log(found);
				//TODO facebook
				res.send({
					comments: found.map(function(obj) {
						var ret = {
							id: obj._id,
							post_id: obj.post_id,
							user_id: {
								user_id: obj.user_id._id
									// username: obj.user_id.username ? obj.user_id.username : obj.user_id.displayName
									// profile_pic_url: app.config.serverUrl + '/image/?id=' + obj.user_id.profile_pic_id
							},
							status: obj.status,
							timestamp: diffrence(obj.timestamp - new Date())
						};
						console.log(obj.user_id);
						if (obj.user_id.username) {
							ret.user_id.username = obj.user_id.username;
						} else {
							ret.user_id.username = obj.user_id.displayName;
						}
						// if (obj.user_id.facebookId) {
						// 	ret.user_id.username = obj.user_id.displayName;
						if (obj.user_id.profile_pic_id) {
							ret.user_id.profile_pic_url = app.config.serverUrl + '/image/?id=' + obj.user_id.profile_pic_id;
						} else if (obj.user_id.profile_pic_url) {
							ret.user_id.profile_pic_url = obj.user_id.profile_pic_url;
						}
						// }
						return ret;
					})
				});
			}
		});
	});

	app.post('/post_personal/comments', function(req, res) {
		post_personal.findOne({
			_id: req.body.post_id,
			removed: false
		}).populate('user_id').exec(function(err, found) {
			if (err) {
				res.send(500);
				console.log(err);
			} else if (!found) {
				res.send(404);
			} else {
				com = new comments({
					post_id: req.body.post_id,
					timestamp: new Date(),
					user_id: req.user._id,
					status: req.body.status
				});
				save_error(com, res, function() {
					post_personal.findOneAndUpdate({_id:com.post_id}, {$inc: {comments_number: 1}}, {multi: false}, function(err, post) {
						// console.log(err, post);
					});
					// found.comments_number += 1;
					// found.markModified('comments_number');
					// save_error(found, res, function() {
					res.send({
						id: com._id
					});
				});
				if(!found.user_id._id.equals(req.user._id)) {
					gcm.notifyOwner(req.user, found);
				}
				comments.find({post_id: req.body.post_id}).populate('user_id').exec(function(err, raw_comments) {
					gcm.notifyCommenters(req.user, raw_comments.map(function(comment) {
						return comment.user_id;
					}), found);
				});
			}
		});
	});
	app.post('/post_personal/comment/delete', function(req, res) {
		comments.findOneAndUpdate({_id: req.body.id, removed:false}, {removed: true}, function(err, comment) {
			if(!err && comment) {
				post_personal.findOneAndUpdate({_id:comment.post_id}, {$inc: {comments_number: -1}}, {multi: false}, function(err, post) {
					console.log(err, post);
				});
				res.send();
			} else {
				res.status(400).send();
			}
		});
	});

	app.post('/post_personal/edit', multipartMiddleware, function(req, res) {
		if(!req.body.status) {
			return res.send(400, 'No status');
		}
		if(req.body.status.length > 2000) {
			return res.send(400, 'Status too long');
		}
		post_personal.findOne({
			_id: req.body.post_id,
			removed: {
				$in: [false, null]
			}
		}, function(err, post) {
			if (!post) {
				res.send(400, {
					err: 'No post'
				});
			} else {
				post.status = req.body.status;
				if (!req.files.image) {
					save_error(post, res, function() {
						res.send("Post edited");
					});
				} else {
					Image.saveImageHttp(req, function(image) {
						post.image_id = image._id;
						save_error(post, res, function() {
							res.send("Post edited");
						});
					});
				}
			}
		});
	});

	var location = {
		type: "Point",
		coordinates: [0, 0]
	};
	var searchOptions = {
		maxDistance: 1000 / 6378137,
		spherical: true
	};

	var magicNumber = 6378137;

	app.post('/post_personal/up_vote', function(req, res) {
		if (!req.body.id) {
			res.send(400);
		} else {
			post_personal.findOne({
				_id: req.body.id,
				removed: {
					$in: [null, false]
				}
			}, function(err, post) {
				console.log(err, post);
				if (err) {
					res.send(500, err);
				} else if (!post) {
					res.send(404);
				} else {
					if (post.up_votes.indexOf(req.user._id) > -1) {
						post.up_votes.splice(post.up_votes.indexOf(req.user._id), 1);
						save_error(post, res, function() {
							res.send({
								response: "not up"
							});
						});
					} else {
						post.up_votes.push(req.user._id);
						save_error(post, res, function() {
							res.send({
								response: "up"
							});
						});
					}
				}
			});
		}
	});

	app.post('/post_personal/down_vote', function(req, res) {
		if (!req.body.id) {
			res.send(400);
		} else {
			post_personal.findOne({
				_id: req.body.id,
				removed: {
					$in: [null, false]
				}
			}, function(err, post) {
				if (err) {
					res.send(500, err);
				} else if (!post) {
					res.send(400);
				} else {
					if (post.down_votes.indexOf(req.user._id) > -1) {
						post.down_votes.splice(post.down_votes.indexOf(req.user._id), 1);
						save_error(post, res, function() {
							res.send({
								response: "not down"
							});
						});
					} else {
						post.down_votes.push(req.user._id);
						save_error(post, res, function() {
							res.send({
								response: "down"
							});
						});
					}
				}
			});
		}
	});


	app.post('/post_personal/delete', function(req, res) {
		post_personal.findOne({
			_id: req.body.post_id,
			user_id: req.user._id
		}, function(err, post) {
			if (err) {
				res.send(500, err);
			} else if (!post) {
				res.send(400);
			} else {
				post.removed = true;
				post.save(function(err) {
					if (err) {
						res.send(500);
					} else {
						res.send(200);
					}
				});
			}
		});
	});

	app.post('/post_personal/id', function(req, res) {
		if (!req.body.id) {
			res.send(400, {
				err: 'No id'
			});
		}
		post_personal.findById(req.body.id).populate('user_id').exec(function(err, post) {
			if (!post || err) {
				console.log(err);
				res.send(400, {
					err: err || 'No such post'
				});
				return;
			}
			console.log(post);
			var a = diffrence(post.timeStamp - new Date());
			var ret = {
				id: post._id,
				image: post.image_id ? app.config.serverUrl + "/image/?id=" + post.image_id : null,
				status: post.status,
				user_id: post.user_id._id,
				username: post.user_id.username || post.user_id.displayName,
				timeStamp: a,
				up_votes: (post.up_votes ? post.up_votes.length : 0) - (post.down_votes ? post.down_votes.length : 0),
				upvoted: post.up_votes ? post.up_votes.indexOf(req.user._id) > -1 : false,
				downvoted: post.down_votes ? post.down_votes.indexOf(req.user._id) > -1 : false,
				comments_number: post.comments_number,
				profile_pic_url: post.user_id.profile_pic_id ? (config.serverUrl + '/image/?id=' + post.user_id.profile_pic_id) : post.user_id.profile_pic_url
			};

			res.send(ret);
		});
	});

	app.post('/post_personal/get', multipartMiddleware, function(req, res) {
		var pageSize = 100;
		var pageNumber = 0;
		if (req.body.pageSize) {
			pageSize = Number(req.body.pageSize);
		}
		if (req.body.pageNumber) {
			pageNumber = Number(req.body.pageNumber);
		}
		var location = {
			type: "Point",
			coordinates: [Number(req.query.x), Number(req.query.y)]
		};
		console.log(pageSize, pageNumber);
		if (!req.body.radius) {
			res.send(404);
		} else
			User.aggregate(
				[{
					"$geoNear": {
						"near": {
							"type": "Point",
							"coordinates": [parseFloat(req.body.x), parseFloat(req.body.y)]
						},
						"distanceField": "distance",
						"maxDistance": Number(req.body.radius),
						"spherical": true,
						"limit": 1000
					},
				}],
				function(err, users) {
					post_personal.find({
						user_id: {
							"$in": users
						},
						removed: {
							$in: [false, null]
						}
					}, {
						status: 1,
						comments_number: 1,
						timeStamp: 1,
						image_id: 1,
						up_votes: 1,
						down_votes: 1,
						user_id: 1,
					}).sort('-timeStamp').skip(pageNumber * pageSize).limit(pageSize).exec(function(err, posts) {
						var pos = [];
						//console.log(posts);
						for (var i = posts.length - 1; i >= 0; i--) {
							var a = diffrence(posts[i].timeStamp - new Date());
							pos.push({
								id: posts[i]._id,
								image: posts[i].image_id ? app.config.serverUrl + "/image/?id=" + posts[i].image_id : null,
								status: posts[i].status,
								user_id: posts[i].user_id,
								timeStamp: a,
								up_votes: (posts[i].up_votes ? posts[i].up_votes.length : 0) - (posts[i].down_votes ? posts[i].down_votes.length : 0),
								upvoted: posts[i].up_votes ? posts[i].up_votes.some(function(id) {
									return id.equals(req.user._id);
								}) : false,
								downvoted: posts[i].down_votes ? posts[i].down_votes.some(function(id) {
									return id.equals(req.user._id);
								}) : false,
								comments_number: posts[i].comments_number
							});
						}
						ids = {};
						//TODO facebook
						console.log(users);
						for (var i = 0; i < users.length; i++) {
							ids[users[i]._id] = {};
							if (users[i].facebookId) {
								if (users[i].username) {
									ids[users[i]._id].username = users[i].username;
								} else {
									ids[users[i]._id].username = users[i].displayName;
								}
								if (users[i].profile_pic_id) {
									ids[users[i]._id].profile_pic_url = users[i].profile_pic_id ? app.config.serverUrl + "/image/?id=" + users[i].profile_pic_id : null;
								} else {
									ids[users[i]._id].profile_pic_url = users[i].profile_pic_url;
								}
							} else {
								ids[users[i]._id] = {
									username: users[i].username || users[i].displayName,
									profile_pic_url: users[i].profile_pic_id ? app.config.serverUrl + "/image/?id=" + users[i].profile_pic_id : null
								};
							}
						}
						res.send({
							posts: pos,
							users: ids
						});
					});
				});
	});

	app.get('/post_personal/my', function(req, res) {
		var pageSize = 100 || req.query.pageSize;
		var pageNumber = 0 || req.query.pageNumber;
		if (req.query.pageSize) {
			pageSize = Number(req.query.pageSize)
		}
		post_personal.find({
			user_id: req.user._id,
			removed: {
				$in: [false, null]
			}
		}).skip(pageNumber * pageSize).limit(pageSize).exec(function(err, posts) {
			var pos = [];
			console.log(posts);
			var users = {};
			if (req.user.username) {
				users[req.user._id] = req.user.username;
			} else {
				users[req.user._id] = req.user.displayName;
			}
			for (var i = posts.length - 1; i >= 0; i--) {
				var a = diffrence(posts[i].timeStamp - new Date());
				pos.push({
					id: posts[i]._id,
					image: posts[i].image_id ? app.config.serverUrl + "/image/?id=" + posts[i].image_id : null,
					status: posts[i].status,
					user_id: posts[i].user_id,
					timeStamp: a,
					up_votes: (posts[i].up_votes ? posts[i].up_votes.length : 0) - (posts[i].down_votes ? posts[i].down_votes.length : 0),
					upvoted: posts[i].up_votes ? posts[i].up_votes.indexOf(req.user._id) > -1 : false,
					downvoted: posts[i].down_votes ? posts[i].down_votes.indexOf(req.user._id) > -1 : false,
					comments_number: posts[i].comments_number ? posts[i].comments_number : 0
				});
			}
			res.send({
				posts: pos
			});
		});
	});

	app.post('/post_personal/user', function(req, res) {
		var pageSize = 100 || req.query.pageSize;
		var pageNumber = 0 || req.query.pageNumber;
		if (req.query.pageSize) {
			pageSize = Number(req.query.pageSize);
		}
		post_personal.find({
			user_id: req.body.user_id,
			removed: {
				$in: [false, null]
			}
		}).skip(pageNumber * pageSize).limit(pageSize).exec(function(err, posts) {
			var pos = [];
			console.log(posts);
			for (var i = posts.length - 1; i >= 0; i--) {
				var a = diffrence(posts[i].timeStamp - new Date());
				pos.push({
					id: posts[i]._id,
					image: posts[i].image_id ? app.config.serverUrl + "/image/?id=" + posts[i].image_id : null,
					status: posts[i].status,
					user_id: posts[i].user_id,
					timeStamp: a,
					up_votes: (posts[i].up_votes ? posts[i].up_votes.length : 0) - (posts[i].down_votes ? posts[i].down_votes.length : 0),
					upvoted: posts[i].up_votes ? posts[i].up_votes.indexOf(req.user._id) > -1 : false,
					downvoted: posts[i].down_votes ? posts[i].down_votes.indexOf(req.user._id) > -1 : false,
					comments_number: posts[i].comments_number ? posts[i].comments_number : 0
				});
			}
			res.send({
				posts: pos
			});
		});
	});
	app.post('/post_personal/report', function(req, res) {
		//req.body.post_id
		//req.body.reason
		post_personal.findById(req.body.post_id, function(err, post) {
			if (err) {
				res.send(500);
			} else if (!post) {
				res.send(400);
			} else {
				report.count({
					user_id: req.user._id,
					post_id: post._id
				}, function(err, count) {
					if (err) {
						res.send(500)
					} else if (count === 0) {
						report.create({
							user_id: req.user._id,
							post_id: post._id,
							reason: req.body.reason
						}, function(err, Report) {
							if (err) {
								res.send(500);
							} else if (!Report) {
								res.send(400);
							} else {
								res.send({
									status: 'Report submited'
								});
								report.count({
									post_id: post._id
								}, function(err, count) {
									if (err) {
										console.log(err)
									} else if (count >= 10) {
										post.removed = true;
										post.save(function(err) {
											if (err) {
												console.log(err);
											} else {
												console.log('Remove post', JSON.stringify(post));
											}
										})
									}
								})
							}
						});
					} else {
						res.send(400, {
							status: 'already reported'
						});
					}
				})
			}
		});
	});
};

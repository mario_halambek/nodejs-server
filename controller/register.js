var crypto = require('crypto');
var rand = require('csprng');
var mongoose = require('mongoose');
var User = require('../models/user');
var FB = require('fb');
var hasher = require('./cry');

exports.registerPassword = function(email, password, gcmToken, res) {
	var x = email;
	var temp = rand(160, 36);
	var token = crypto.createHash('sha512').update(email + rand).digest("hex");
	var hashed_password = hasher(temp, password);
	var newuser = new User({
		token: token,
		email: email,
		hashed_password: hashed_password,
		salt: temp,
		gcmToken: [gcmToken]
	});
	User.find({
		email: email,
		deleted: false
	}, function(err, users) {
		if(err) {
			return res.status(500).send();
		}
		var len = users.length;
		if (len === 0) {
			newuser.save(function(err) {
				if(err) {
					return res.status(500).send();
				}
				res.json({
					'response': "Sucessfully Registered"
				});
			});
		} else {
			res.writeHead(400, {
				'response': "Username already Registered"
			});
			res.end();
		}
	});

};

exports.registerFacebook = function(accessToken, gcmToken, res) {
	if (!accessToken) {
		return res.send(400, {
			err: "no facebook accessToken"
		});
	}
	FB.setAccessToken(accessToken);
	FB.api('/me', function(facebookResponse) {
		if (facebookResponse.error) {
			return res.send(400, facebookResponse.error);
		}
		var token = crypto.createHash('sha512').update(facebookResponse.name + rand).digest("hex");
		User.findOne({
			facebookId: facebookResponse.id
		}, function(err, user) {
			if (err) {
				return res.send(500, {
					err: err
				});
			}
			console.log(user);
			if (user) {
				return res.json({
					'response': "Already Registered"
				});
			}
			User.create({
				facebookId: facebookResponse.id,
				token: token,
				displayName: facebookResponse.name,
				gcmToken: [gcmToken],
				profile_pic_url: 'https://graph.facebook.com/' + facebookResponse.id + '/picture?type=large'
			}, function(err, user) {
				if (err || !user) {
					return res.send(500, {
						err: err || 'No user'
					});
				}
				res.json({
					'response': "Sucessfully Registered"
				});
			});
		});
	});
};

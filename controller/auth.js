var user = require('../models/user');

module.exports.auth = function (req, res, callback) {
	if ( !req.headers.authorization) {
		return res.status(400).send();
	}
	user.find({token: req.headers.authorization}, function(err, users) {
		if (err) {
			console.err(err);
			return res.status(400).send();
		}
		if (users.length === 0) {
			console.log('invalid token');
			res.send(401, "Invalid token.");
		} else if(users[0].last_login_date < Date.now() - 1000 * 3600) {
			console.log('expired token');
			res.send(401, 'Token expired reauthorize');
		}else if(users[0].baned) {
			res.status(403).send();
		} else {
			callback(req, res, users[0]);
		}
	});
};


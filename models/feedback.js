var mongoose = require('mongoose');

var feedbackSchema = new mongoose.Schema({
	text: {
		type: String,
		required: true
	},
	user: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		ref: 'users'
	}
});

var Feedback = mongoose.model('feedback', feedbackSchema);

module.exports = Feedback;

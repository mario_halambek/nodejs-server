var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Validator = require('validator');

var userSchema = mongoose.Schema({
	token: {
		type: String,
		index: 'unique'
	},
	gcmToken: {
		type: [String],
		default: []
	},
	last_login_date: {
		type: Date
	},
	username: {
		type: String,
		index: 'unique'
	},
	facebookId: {
		type: String,
		index: 'unique'
	},
	displayName: {
		type: String
	},
	profile_pic_id: {
		type: Schema.ObjectId
	},
	profile_pic_url: {
		type: String
	},
	hashed_password: {
		type: String
	},
	email: {
		type: String
	},
	deleted: {
		type: Boolean,
		default: false
	},
	about: String,
	salt: String,
	temp_str: String,
	coords: {
		type: [Number],
		index: '2dsphere'
	}
});

module.exports = mongoose.model('users', userSchema);

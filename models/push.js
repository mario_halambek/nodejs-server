var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var pushSchema = mongoose.Schema({
	timestamp: {
		type: Date,
		default: Date.now()
	},
	seen: {
		type: Boolean,
		default: false
	},
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'users',
		required: true,
		index: true
	},
	postId: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		index: true
	}
});

module.exports = mongoose.model('push', pushSchema);

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var reportSchema = Schema({
	user_id: {
		ref: 'users',
		type: Schema.ObjectId,
		required: true
	},
	post_id: {
		type: Schema.ObjectId,
		required: true
	},
	reason: {
		type: String
	},
	timestamp: {
		type: Date,
		default: Date.now
	}
});

module.exports = mongoose.model('report', reportSchema);

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var postPersonalSchema = mongoose.Schema({
	image_id: Schema.ObjectId,
	user_id: {
		type: Schema.ObjectId,
		ref: 'users',
		required: true
	},
	timeStamp: {
		type: Date,
		default: Date.now
	},
	up_votes: {
		type: [Schema.ObjectId],
		default: []
	},
	down_votes: {
		type: [Schema.ObjectId],
		default: []
	},
	status: {
		type: String
	},
	name: String,
	comments_number: {
		type: Number,
		default: 0
	},
	removed: {
		type: Boolean,
		default: false
	}
});

module.exports = mongoose.model('posts_personal', postPersonalSchema);

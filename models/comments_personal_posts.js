var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentsSchema = mongoose.Schema({
	post_id: {
		type: Schema.ObjectId,
		required: true,
		index: true
	},
	timestamp: {
		type: Date,
		default: Date.now 
	},
	user_id: {
		type: Schema.ObjectId,
		ref: 'users',
		required: true,
		index: true
	},
	status: {
		type: String,
		required: true
	},
	removed: {
		type: Boolean,
		default: false
	}
});

module.exports = mongoose.model('comments', commentsSchema);

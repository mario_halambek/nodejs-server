var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var panorama = new Schema({
	coords: {
		type: [Number],
		index: '2dsphere',
		required: true
	},
	url: {
		type: String,
		required: true
	}
});

var Panorama = mongoose.model('panorama', panorama);

module.exports = Panorama;

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var postLocalSchema = mongoose.Schema({
	name: String,
	status: String,
	image_id: {
		type: Schema.ObjectId
	},
	user_id: {
		type: Schema.ObjectId,
		ref: 'users',
		required: true
	},
	timeStamp: {
		type: Date,
		default: Date.now
	},
	up_votes: {
		type: [Schema.ObjectId],
		default: []
	},
	down_votes: {
		type: [Schema.ObjectId],
		default: []
	},
	coords: {
		type: [Number],
		index: '2dsphere'
	},
	comments_number: {
		type: Number,
		default: 0
	},
	removed: {
		type: Boolean,
		default: false
	}
});

module.exports = mongoose.model('posts_local', postLocalSchema);

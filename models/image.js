var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var fs = require('fs');


var imageSchema = mongoose.Schema({
	data: {
		type: Buffer,
		required: true
	},
	contentType: {
		type: String,
		required: true
	}
});

var Image = mongoose.model('images', imageSchema);

Image.saveImageHttp = function(req, callback) {
	var newImage = new Image();

	fs.readFile(req.files.image.path, function(err, data) {
		if (err) {
			return next(err);
		}
		newImage.data = data;

		fs.unlink(req.files.image.path);

		newImage.contentType = req.files.image.headers['content-type'];

		newImage.save(function(err) {
			if (err) {
				return next(err);
			} else {
				callback(newImage);
			}
		});

	});
};

module.exports = Image;

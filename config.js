module.exports = {
	serverUrl : 'https://server.gatherspot.me',
	log: {
		info: './logs/info.log',
		debug: './logs/debug.log',
		err: './logs/err.log',
		size: {
				info: 20 * 1024 * 1024, //20MB
				debug: 20 * 1024 * 1024,
				err: 20 * 1024 * 1024
		}
	}
};

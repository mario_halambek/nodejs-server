#!/usr/bin/env node

var gcm = require('node-gcm');

var sender = new gcm.Sender('AIzaSyCF1cVJB1JXBvJO_JMM0I5hHn1hQkGJCEM');
var mongoose = require('mongoose');
var Notification = require('./models/push');

function notifyCommenters (user, users, post) {
	Notification.find({
		userId:{
			$in:users
		}
	}, function(err, notifications) {
		if(err) {
			return console.err(err);
		}
		var userIds = notifications.map(function(notification){
			return notification.userId.toString();
		});
		console.log('userIds', userIds);
		var sendingUsersIds = [];
		var gcmTokens = users.reduce(function(prev, curr,index, arr) {
			if(userIds.indexOf(curr._id.toString()) === -1 && !post.user_id.equals(curr._id) && !curr._id.equals(user._id)) {
				curr.gcmToken.forEach(function(token) {
            	if(arr.indexOf(token) === -1) {
						arr.push(token);
					}
				});
				if(sendingUsersIds.indexOf(curr._id) === -1) {
					sendingUsersIds.push(curr._id);
				}
			}
			return arr;
		}, []);
		var message = new gcm.Message();
		message.addData('message', (user.username || user.displayName) + ' commented on post you also commented.');
		message.addData('id', post._id); 
		message.addData('title', 'New comment');
		message.addData('msgcnt', '3'); // Shows up in the notification in the status bar
		message.addData('soundname', 'beep.wav'); //			
		message.timeToLive = 3000; // Duration
		message.addData('id', post._id);
		if (post.coords) {
			message.addData('type', 'local');
		} else {
			message.addData('type', 'personal');
		}
		sender.sendNoRetry(message, gcmTokens, function(err, result) {
		//console.log(err, result);
		if (!err) {
			console.log('sent user notification to users with result', JSON.stringify(result));
			var notifications = sendingUsersIds.map(function(userId) {
				return {
					postId: post._id,
					userId: userId
				};
			});
			Notification.create(notifications, function (err, notifications) {
				if(err) {
					console.log(err);
				} else {
					console.log('Saved sent notifications for users');
				}
			});
		} else {
			console.err('Failed to send noification to users', err, JSON.stringify(result));
		}
	});
});
}
function notifyOwner(user, post) {
	console.log('Want to send Push notification to ', user._id);
	//console.log(post.user_id , user._id);
	if (post.user_id === user._id) {
		return;
	}
	if (user.gcmToken) {
		Notification.findOne({
			postId: post._id,
			userId: post.user_id._id
		}, function(err, cond) {
			console.log(err, cond);
			if (err || cond) {
				console.log(err || 'User already has notification regarding this post');
				return;
			}
			var message = new gcm.Message();
			message.addData('message', (user.username || user.displayName) + ' commented on your post.');
			message.addData('title', 'New comment');
			message.addData('msgcnt', '3'); // Shows up in the notification in the status bar
			message.addData('soundname', 'beep.wav'); //
			message.timeToLive = 3000; // Duration
			message.addData('id', post._id);
			if (post.coords) {
				message.addData('type', 'local');
			} else {
				message.addData('type', 'personal');
			}
			console.log('Sending message to user', JSON.stringify(message));
			console.log(post.user_id.gcmToken);
			sender.sendNoRetry(message, post.user_id.gcmToken, function(err, result) {
				//console.log(err, result);
				if (!err) {
					console.log('sent user notification to user', post.user_id._id, 'with result', JSON.stringify(result));
					Notification.create({
						postId: post._id,
						userId: post.user_id._id
					}, function (err, notification) {
						if(err) {
							console.log(err);
						} else {
							console.log('Saved sent notification for user', notification._id);
						}
					});
				} else {
					console.err('Failed to send noification to user', user.displayName, err, result);
				}
			});
		});
	}
}
module.exports = {
	notifyOwner: notifyOwner,
   notifyCommenters: notifyCommenters
};

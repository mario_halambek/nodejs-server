var Panorama = require('../models/panorama.js');
var http = require('http');
var querystring = require('querystring');

function near(coordinate, cb) {
	Panorama.geoNear({
		type:'Point',
		coordinates: coordinate
	},{
		maxDistance: 1000,
		distanceMultiplier: 6372137,
		spherical: true,
		num: 1
	}, function(err, panoramas) {
		console.log(err, panoramas);
		var query = {
			set: 'public',
			from: 0,
			to: 100,
			minx: coordinate[0] - 0.01,
			miny: coordinate[1] - 0.01,
			maxx: coordinate[0] + 0.01,
			maxy: coordinate[1] + 0.01,
			mapfilter: true,
			size: 'original'
		};
		query = querystring.stringify(query);
		if(panoramas.length === 0) {
			var url = 'http://www.panoramio.com/map/get_panoramas.php?' + query;
			var req = http.request(url,function(res) {
				var o = '';
				res.on('data', function(data){
					o += data;
				});
				res.on('end', function() {
					panoramas = JSON.parse(o).photos.map(function(photo) {
						return {
							coords: [photo.longitude, photo.latitude],
							url: photo.photo_file_url
						};
					});
					if(panoramas.length !== 0) {
						Panorama.create(panoramas, function(err) {
							if(err) {
								console.log(err);
								return cb(null);
							}
							cb(panoramas[0].url);
						});
					} else {
						cb(null);
					}
				});
			});
			req.on('error', function(e) {
				console.log(e.message);
				return cb(null);
			});
			req.end();
		} else {
			cb(panoramas[0].obj.url);
		}
	});
}

module.exports = near;

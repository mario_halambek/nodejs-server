'use strict';

module.exports = function(logger) {
	console.debug = function() {
		var args = [];
		for (var i = 0; i < arguments.length; i++) {
			args.push(arguments[i]);
		}
		logger.debug(args.join(' '));
		logger.stream.write('DEBUG', args.join(' '));
	};
	console.err = function() {
		var args = [];
		for (var i = 0; i < arguments.length; i++) {
			args.push(arguments[i]);
		}
		logger.stream.write('ERR', args.join(' '));
		logger.error(args.join(' '));
		console.trace(args.join);
	};
	console.log = function() {
		var args = [];
		for (var i = 0; i < arguments.length; i++) {
			args.push(arguments[i]);
		}
		logger.info(args.join(' '));
		logger.stream.write('LOG', args.join(' '));
	};
};

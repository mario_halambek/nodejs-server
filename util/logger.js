'use strict';

var winston = require('winston'),
	fs = require('fs'),
	config = require('../config');

winston.emitErrs = true;

var logsDirs = [config.log.info, config.log.err];

logsDirs.forEach(function(dir) {
	dir = dir.substring(0, dir.lastIndexOf('/'));
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
	}
});


var logger = new winston.Logger({
	transports: [
		new winston.transports.DailyRotateFile({
			//datePattern: ''
			name: 'info-file',
			level: 'info',
			filename: config.log.info,
			handleExceptions: false,
			json: true,
			maxsize: config.log.size.info,
			maxFiles: 5,
			colorize: true
		}),
		new winston.transports.Console({
			name: 'debug-file',
			level: 'debug',
			filename: config.log.debug,
			handleExceptions: false,
			json: true,
			colorize: true
		}),
		new winston.transports.DailyRotateFile({
			name: 'error-file',
			level: 'error',
			maxsize: config.log.size.err,
			filename: config.log.err,
			handleExceptions: false,
			json: true,
			colorize: true
		}),
		new winston.transports.Console({
			name: 'log-file',
			level: 'log',
			handleExceptions: false,
			json: true,
			colorize: true
		})
	],
	exitOnError: true
});

module.exports = logger;
module.exports.stream = {
	write: function(message, encoding) {
		logger.info(message);
		fs.appendFile('./logMain.log', message, function(err, data) {
		});
	}
};
